nums = [4, 6, 8, 24, 12, 2]
print("zadanie 1: najwiekszy element z listy bez uzycia sort() i print max(nums)")


def highest_element(nums):
    first = nums[0]
    for iterator in nums:
        if first <= iterator:
            first = iterator
        else:
            continue
    return first


print(highest_element(nums))
print("\nZadanie 2: fizzbuzz")


def fizz_buzz(number: int):
    if number % 3 == 0 and number % 5 == 0:
        print("FizzBuzz")
    elif number % 5 == 0:  ## jesli liczba jest podzielna przez 5
        print("Buzz")
    elif number % 3 == 0:  ## jesli liczba jest podzielna przez 3
        print("Fizz")
    else:
        print(number)


fizz_buzz(3)
fizz_buzz(5)
fizz_buzz(15)
fizz_buzz(4)

print("\nZadanie 3: mnozenie argumentow o dowolnej ilosci")


def product(*factors: int):
    result = 1
    for i in factors:
        result *= i
    return result


print(product(3, 123412312, 3, 4, 5, 1, 2))
