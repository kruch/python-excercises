from os import name


class Student:

    def __init__(self, name, surname, student_id, specialization, university):
        self.name = name
        self.surname = surname
        self.student_id = student_id
        self.specialization = specialization
        self.university = university

    def print_info(self):
        print(f"{self.name}, {self.surname}, {self.student_id}, {self.specialization}, {self.university}")


Janusz = Student("Janusz", "Turecki", "123411", "Data Science", "University of Gdansk")
Janusz.print_info()
