class Car:

    def __init__(self, max_speed: float, mileage: float, engine: str, drive: str):
        self.max_speed = max_speed
        self.mileage = mileage
        self.engine = engine
        self.drive = drive

    def add_mileage(self, distance: float, ):
        self.mileage += distance


my_car = Car(190.0, 170000.0, "2.0 TDI", "front wheels")
print(my_car.mileage)
my_car.add_mileage(231123123.2)
print(my_car.mileage)
